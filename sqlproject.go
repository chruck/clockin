// From the tutorial by Nic Raboy:
// "Using A SQLite Database For Local Data In A Golang Application"
// https://www.youtube.com/watch?v=YpDVQC8hfik

package main
/*

import ("database/sql"  // sql.Open(), database.Prepare(), database.Query(), rows.Next(), rows.Scan()
        _ "github.com/mattn/go-sqlite3"  // for sqlite3
        "fmt"  // fmt.Println()
        "strconv"  // strconv.Itoa()
)

func main() {
    // Open db file
    database, _ := sql.Open("sqlite3", "./nraboy.db")

    // Create database
    statement, _ := database.Prepare("CREATE TABLE IF NOT EXISTS people (id INTEGER PRIMARY KEY, firstname TEXT, lastname TEXT)")
    statement.Exec()

    // Add data
    statement, _ = database.Prepare("INSERT INTO people (firstname, lastname) VALUES (?, ?)")
    //statement.Exec("Nic", "Raboy")
    //statement.Exec("Maria", "Raboy")
    statement.Exec("John", "Doe")

    // Query
    //rows, _ := database.Query("SELECT id, firstname, lastname FROM people")
    rows, _ := database.Query("SELECT id, firstname FROM people")
    var id int
    var firstname string
    //var lastname string
    for rows.Next() {
        //rows.Scan(&id, &firstname, &lastname)
        rows.Scan(&id, &firstname)
        //fmt.Println(strconv.Itoa(id) + ": " + firstname + " " + lastname)
        fmt.Println(strconv.Itoa(id) + ": " + firstname)
    }
}
*/
