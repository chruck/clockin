// Package main is the overall package for the 'clockin' program
package main

import (
        "database/sql"                  // sql.Open(), database.Query()
        _ "github.com/mattn/go-sqlite3" // for sqlite3
        "fmt"                           // fmt.Println()
        "log"                           // log.Fatal()
        "path/filepath"                 // filepath.Glob()
        //"bufio"                         // bufio.NewReader()
        //"os"                            // os.Stdin
        //"strings"                       // strings.Replace()
        "time"                          // time.Date(), time.Now()
        "strconv"                       // strconv.ParseFloat()
        "flag"                          // flag.Bool
)

// Global vars:
var debugVar bool = false  // Used to print out debug information

func getToday() (today time.Time) {
        today = time.Now()

        debug("Current time is ", today.String())

        year, month, day := today.Date()
        today = time.Date(year, month, day, 00, 00, 00, 0, time.UTC)

        debug("Current time midnight is", today.String(), "\n")

        return
}

func recentSaturday(today time.Time) (pastSat time.Time) {
        // iterate back to Saturday
        pastSat = today
        for time.Saturday != pastSat.Weekday() {
                pastSat = pastSat.AddDate(0, 0, -1)
                debug("Iterated time is: ", pastSat.String())
        }

        year, month, day := pastSat.Date()
        debug("Past Saturday is ", pastSat.String())
        debug("\tYear:  ", strconv.Itoa(year))
        debug("\tMonth:  ", month.String())
        debug("\tDay:  ", strconv.Itoa(day))

        //pastSat = time.Date(year, month, day, 00, 00, 00, 0, time.UTC)

        return
}

func lastSaturday(pastSat time.Time) (lastSat time.Time) {
        lastSat = pastSat.AddDate(0, 0, -7)
        year, month, day := lastSat.Date()
        debug("Saturday before is ", lastSat.String())
        debug("\tYear:  ", strconv.Itoa(year))
        debug("\tMonth:  ", month.String())
        debug("\tDay:  ", strconv.Itoa(day))

        return
}

func findDbFile() string {
        // Look for PunchClock database
        files, err := filepath.Glob("PunchClock_*.db")

        if nil != err {
                log.Fatal(err)
        }

        for idx, fname := range files {
                debug(strconv.Itoa(idx), ": ", fname)
        }

        lastfile := files[len(files) - 1]

        debug("Last is " + lastfile)

        return lastfile
}

func selectStmntHours(start, end time.Time) (query string) {
        query = `SELECT (julianday(endTime)-julianday(startTime))*24
                 FROM WorkTimes
                 WHERE startTime BETWEEN date('`
        query += start.Format(time.RFC3339) + "') AND date('"
        query += end.Format(time.RFC3339) + "');"

        debug("Query string:\n\t", query)

        return
}

func selectStmntForDisplay(start, end time.Time) (query string) {
        query = `SELECT endTime,startTime,
                         (julianday(endTime)-julianday(startTime))*24
                 FROM WorkTimes
                 WHERE startTime BETWEEN date('`
        query += start.Format(time.RFC3339) + "') AND date('"
        query += end.Format(time.RFC3339) + "');"

        debug("Query string:\n\t", query)

        return
}

func queryDb(db string) {
        // Open db file
        database, err := sql.Open("sqlite3", "./" + db)

        if nil != err {
                log.Fatal(err)
        }

        today := getToday();
        pastSat := recentSaturday(today)
        lastSat := lastSaturday(pastSat)
        lastlastSat := lastSat.AddDate(0, 0, -7)

        fmt.Println("\nLast Last week:\n")

        // Display the table
        query := selectStmntForDisplay(lastlastSat, lastSat)

        rows, err := database.Query(query)

        if nil != err {
                log.Fatal(err)
        }

        var endTime string
        var startTime string
        var diffTime string

        for rows.Next() {
                rows.Scan(&endTime, &startTime, &diffTime)
                fmt.Println(endTime + " " + startTime + " " + diffTime)
        }

        // Display just the hours:
        query = selectStmntHours(lastlastSat, lastSat)

        rows, err = database.Query(query)

        if nil != err {
                log.Fatal(err)
        }

        totalhrs := 0.0
        diffhrs  := 0.0
        for rows.Next() {
                err = rows.Scan(&diffTime)

                if nil != err {
                        log.Fatal(err)
                }

                diffhrs, err = strconv.ParseFloat(diffTime, 64)
                if nil != err {
                        log.Fatal(err)
                }

                totalhrs += diffhrs
                //fmt.Println(diffTime + " --> " + totalhrs)
                //fmt.Sprint(diffTime + " --> ", totalhrs)
                fmt.Printf("%f --> %f\n", diffhrs, totalhrs)
        }

        fmt.Println("\nLast week:\n")

        query = selectStmntForDisplay(lastSat, pastSat)

        rows, err = database.Query(query)

        if nil != err {
                log.Fatal(err)
        }

        for rows.Next() {
                rows.Scan(&endTime, &startTime, &diffTime)
                fmt.Println(endTime + " " + startTime + " " + diffTime)
        }

        // Display just the hours:
        query = selectStmntHours(lastSat, pastSat)

        rows, err = database.Query(query)

        if nil != err {
                log.Fatal(err)
        }

        totalhrs = 0.0
        for rows.Next() {
                err = rows.Scan(&diffTime)

                if nil != err {
                        log.Fatal(err)
                }

                diffhrs, err = strconv.ParseFloat(diffTime, 64)
                if nil != err {
                        log.Fatal(err)
                }

                totalhrs += diffhrs
                //fmt.Println(diffTime + " --> " + totalhrs)
                //fmt.Sprint(diffTime + " --> ", totalhrs)
                fmt.Printf("%f --> %f\n", diffhrs, totalhrs)
        }

        fmt.Println("\nThis week so far:\n")

        query = selectStmntForDisplay(pastSat, today)

        rows, err = database.Query(query)

        if nil != err {
                log.Fatal(err)
        }

        for rows.Next() {
                rows.Scan(&endTime, &startTime, &diffTime)
                fmt.Println(endTime + " " + startTime + " " + diffTime)
        }

        // Display just the hours:
        query = selectStmntHours(pastSat, today)

        rows, err = database.Query(query)

        if nil != err {
                log.Fatal(err)
        }

        totalhrs = 0.0
        for rows.Next() {
                err = rows.Scan(&diffTime)

                if nil != err {
                        log.Fatal(err)
                }

                diffhrs, err = strconv.ParseFloat(diffTime, 64)
                if nil != err {
                        log.Fatal(err)
                }

                totalhrs += diffhrs
                //fmt.Println(diffTime + " --> " + totalhrs)
                //fmt.Sprint(diffTime + " --> ", totalhrs)
                fmt.Printf("%f --> %f\n", diffhrs, totalhrs)
        }
}

func askLastweek() float32 {
        fmt.Print("What is the value of lastweek? ")

        /*
        reader := bufio.NewReader(os.Stdin)

        text, err := reader.ReadString('\n')

        if nil != err {
                log.Fatal(err)
        }
        */

        // Convert CRLF to LF:
        //lastweek := strings.Replace(text, "\n", "", -1)
        var i float32
        fmt.Scan(&i)

        return i
}

func debug(msg ...string) {
        if debugVar {
                fmt.Println(msg)
        }
}

func main() {
        // Debug flag:
        flag.BoolVar(&debugVar, "d", false, "Add debug output")
        flag.Parse()

        lastfile := findDbFile();
        queryDb(lastfile);
        lastweek := askLastweek();

        fmt.Println("Last week is:  ", lastweek)
}
