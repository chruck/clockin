NOTES
---
- Started out learning how to use sqlite in Go:
        https://www.youtube.com/watch?v=YpDVQC8hfik
        - Create sqlproject.go
                - Since it imports "github.com/mattn/go-sqlite3", went
                  to that page to discover I need to:
                        go get github.com/mattn/go-sqlite3
        - go build
                - creates binary "clockin" (based on dir name?)
                - could also use:
                        go build sqlproject.go  (creates bin 'sqlproject')
                  but not:
                        go build sqlproject
                        go build main
        - execute:  ./clockin
                - creates empty database file "nraboy.db"
        - video ignores errors when calling functions, by having
          second var be '_'.  eg:
                database, _ := sql.Open(...)
        - for INSERT, using '?' for "parameterized query" to avoid SQL
          injection

- Syntax pickiness:
        - func needs brace cuddled
        - video's IDE shows names of parameters to functions, but they
          should not be typed in
        - strings do not carry over to the next line
        - unused variables are compile-time errors
